Mail
---

This composer package wrapper and simple template helper for PHPMailer.

### Usage: ###

```
#!php
<?php
require 'vendor/autoload.php';

use opensaucesystems\mail\helper as MailHelper;

/**
 * Initiate MailHelper object
 */
$mail = new MailHelper;

/**
 * Enable debug output
 */
$mail->set('debug', true);

/**
 * Set connection
 */
$mail->set(
    'connection',
    array(
        'smtp-host' => 'smtpcorp.com',
        'smtp-port' => '2525',
        'smtp-user' => '',
        'smtp-pass' => ''
));

/**
 * Set email message
 */
$mail->set(
    'message',
    array(
        'recipients' => array(
            array(
                'name'  => 'Lawrence Cherone',
                'email' => 'lawrence@opensauce.systems',
            ),
        ),
        'from'      => array(
            'name'  => 'Website',
            'email' => 'noreply@opensauce.systems',
        ),
        'reply'     => array(
            'name'  => 'Website',
            'email' => 'noreply@opensauce.systems',
        ),
        'subject'   => 'Email testing',
        'body'      => '<p>We are testing our email settings settings or server.</p>'.
                       '<p>If you were lucky enough to receive this email then our check has been successful and you can safely discard this email.</p>',
        //'attachments' => array('opensauce.systems.pdf'),
    )
);

/**
 * Set message body wrapped within a html template
 */
$mail->template('vendor/opensaucesystems/mail/src/templates/default', [
    'title'  => 'Opensauce.systems',
    'website'  => 'http://opensauce.systems',
    'logo'   => 'http://opensauce.systems/logo.png',
    //'body'   => 'Hello email body o-0',
    'footer' => '
        <p>Application Name &copy; <a href="http://opensauce.systems">Open Sauce Systems Limited</a>, registered company in England and Wales, number 7006487</p>
        <p>Registered office: 1 Gloster Court, Whittle Avenue, Segensworth West, Fareham, PO15 5SH</p>
        <p><a target="_blank" href="http://opensauce.systems/images/oss_terms.pdf">terms and conditions</a> | <a href="http://opensauce.systems/main/privacy.php">privacy statement</a></p>
    ',
]);

//echo '<pre>'.print_r($mail, true).'</pre>';die;

/**
 * Try to Send email
 */
try {
    if ($mail->send()) {
        echo '<span style="color:green">successful</span>';
    } else {
    	echo '<span style="color:red">unsuccessful</span>';
    	echo $mail->mail->ErrorInfo;
    }
} catch (Exception $e) {
    /**
     * Error occured, handle the application flow with the appropriate errors
     */
     echo '<pre>'.print_r($e, true).'</pre>';
     echo '<pre>'.print_r($mail, true).'</pre>';
}
```
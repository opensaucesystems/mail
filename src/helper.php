<?php
namespace opensaucesystems\mail {

    use \PHPMailer\PHPMailer\PHPMailer;
    use \Exception;

    /**
     * Open Sauce Systems encapsulation helper class for sending emails.
     *
     * @author: Lawrence Cherone <lawrence@opensauce.systems>
     * @uses PHPMailer
     */
    class helper {

        public $state;
        public $error;
        public $mail;

        /**
         * Construct
         *
         * Set default config structure
         */
        public function __construct()
        {
            $this->config = [
                'debug' => false,
                'connection' => [
                    'smtp-host' => '',
                    'smtp-port' => '',
                    'smtp-user' => '',
                    'smtp-pass' => ''
                ],
                'message' => [
                    'recipients' => [
                        [
                            'name'  => '',
                            'email' => '',
                        ],
                    ],
                    'from'      => [
                        'name'  => '',
                        'email' => '',
                    ],
                    'reply'        => [
                        'name'  => '',
                        'email' => '',
                    ],
                    'subject'     => '',
                    'body'        => '',
                    'attachments' => [],
                ],
            ];
        }

        /**
         * Set/overide config value
         *
         * @param string $key
         * @param string $value
         */
        public function set($key, $value)
        {
            if (!empty($key) && !empty($value)) {
                $this->config[$key] = $value;
            }

            return $this;
        }

        /**
         *
         * @param string $view
         * @param array $data
         */
        public function template($view = null, $data = [])
        {
            // set path
            $this->path = $view . '.php';

            // check view exists or throw exception
            try {
                if (file_exists($this->path) === false) {
                    throw new Exception(
                        'Error: ['.htmlentities($view).'] Template not found: ' . htmlentities($this->path)
                    );
                }
            } catch (Exception $e) {
                throw $e;
            }

            // extract out current message values
            extract($this->config['message']);

            // extract $data passed to this method, which will overide
            if (!empty($data) && is_array($data)) {
                extract($data);
            }

            // start output buffering to catch and return content once parse
            ob_start();

            // require the template
            require_once $this->path;

            // set email body with new template body
            $this->config['message']['body'] = ob_get_clean();

            return $this;
        }

        /**
         * Send function, handles the PHPMailer part
         *
         * @param string $to
         * @param string $from
         * @param string $subject
         * @param string $body
         */
        public function send($to = null, $from = null, $subject = null, $body = null)
        {
            $this->mail = is_object($this->mail) ? $this->mail : new PHPMailer;

            try {
                // set PHPMailer to use SMTP
                $this->mail->isSMTP();

                // debug
                if ($this->config['debug'] === true) {
                    $this->mail->SMTPDebug = 2;
                    $this->mail->Debugoutput = 'html';
                }

                // set SMTP connection host
                $this->mail->Host = $this->config['connection']['smtp-host'];

                // set SMTP connection port
                $this->mail->Port = $this->config['connection']['smtp-port'];

                // set SMTP connection encryption to tls
                $this->mail->SMTPSecure = 'tls';

                // set SMTP connection authentication
                $this->mail->SMTPAuth = true;

                // set SMTP connection username
                $this->mail->Username = $this->config['connection']['smtp-user'];

                // set SMTP connection password
                $this->mail->Password = $this->config['connection']['smtp-pass'];

                // set email from
                if (!empty($from)) {
                    if (is_array($from) && !empty($from['name']) && !empty($from['email'])) {
                        $this->mail->setFrom($from['email'], $from['name']);
                    } elseif (is_array($from) && !empty($from['email'])) {
                        $this->mail->setFrom($from['email']);
                    } elseif (is_array($from)) {
                        throw new Exception('Error: If $from is set as array then it must contain an email and or a name key');
                    }
                } else {
                    if (!empty($this->config['message']['from']['email']) && !empty($this->config['message']['from']['name'])) {
                        $this->mail->setFrom($this->config['message']['from']['email'], $this->config['message']['from']['name']);
                    } elseif (!empty($this->config['message']['from']['email'])) {
                        $this->mail->setFrom($this->config['message']['from']['email']);
                    } else {
                        throw new Exception('Error: $this->config[\'message\'][\'from\'][\'email\'] must be set');
                    }
                }

                // set reply
                if (!empty($this->config['message']['reply']['email']) && !empty($this->config['message']['reply']['name'])) {
                    $this->mail->addReplyTo($this->config['message']['reply']['email'], $this->config['message']['reply']['name']);
                } elseif(!empty($this->config['message']['reply']['email'])){
                    $this->mail->addReplyTo($this->config['message']['reply']['email']);
                }

                // set to
                if (!empty($to)) {
                    if (is_array($to)) {
                        foreach ($to as $recipient) {
                            $this->mail->addAddress($recipient);
                        }
                    } else {
                        $this->mail->addAddress($to);
                    }
                } else {
                    if (is_array($this->config['message']['recipients'])) {
                        foreach ($this->config['message']['recipients'] as $recipient) {
                            if (!empty($recipient['email'])) {
                                $this->mail->addAddress($recipient['email']);
                            } else {
                                 throw new Exception('Error: $this->config[\'message\'][\'recipients\'][*][\'email\'] must be set');
                            }
                        }
                    } elseif (!empty($this->config['message']['recipients'])) {
                        $this->mail->addAddress($this->config['message']['recipients']);
                    } else {
                        throw new Exception('Error: $this->config[\'message\'][\'recipients\'] must be set');
                    }
                }

                // add any file attachments
                if (!empty($this->config['message']['attachments'])) {
                    foreach ($this->config['message']['attachments'] as $attachment) {
                        if (file_exists($attachment)) {
                            $this->mail->addAttachment($attachment);
                        }
                    }
                }

                // set subject
                if (!empty($subject)) {
                    if (!is_array($subject)) {
                        $this->mail->Subject = $subject;
                    } else {
                        throw new Exception('Error: If $subject must be a string');
                    }
                } else {
                   if (!empty($this->config['message']['subject'])) {
                        $this->mail->Subject = $this->config['message']['subject'];
                    } else {
                        throw new Exception('Error: $this->config[\'message\'][\'subject\'] must be set');
                    }
                }

                // set body
                if (!empty($body)) {
                    $this->mail->msgHTML($body);
                    $this->mail->AltBody = strip_tags($body);
                } elseif (!empty($this->config['message']['body'])) {
                    $this->mail->msgHTML($this->config['message']['body']);
                    $this->mail->AltBody = strip_tags($this->config['message']['body']);
                } else {
                    throw new Exception('Error: $this->config[\'message\'][\'body\'] must be set');
                }

                // send email
                if (!$this->mail->send()) {
                    // error
                    return false;
                } else {
                    // success
                    return true;
                }
            } catch (Exception $e) {
                throw $e;
            }
        }
    }
}

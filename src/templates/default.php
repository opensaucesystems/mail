<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= @$title ?></title>
        <style type="text/css">
            html,
            body {
                margin: 0;
                padding: 0;
                height: 100% !important;
                width: 100% !important;
            }
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            .ExternalClass {
                width: 100%;
            }
            table,
            td {
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
            }
            table {
                border-spacing:0 !important;
            }
            .ExternalClass,
            .ExternalClass * {
                line-height: 100%;
            }
            table {
                border-collapse: collapse;
                margin: 0 auto;
            }
            img {
                -ms-interpolation-mode:bicubic;
            }
            .yshortcuts a {
                border-bottom: none !important;
            }
            .mobile-link--footer a {
                color: #666666 !important;
            }
            img {
                border:0 !important;
                outline:none !important;
                text-decoration:none !important;
            }
            hr {
                border: 0;
                height: 1px;
                background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
            }
            @media only screen and (min-width: 601px) {
                .email-container {
                    width: 600px !important;
                }
            }
            @media only screen and (max-width: 600px) {
                .email-container {
                    width: 100% !important;
                    max-width: none !important;
                }
            }
        </style>
    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#E8E8E8" style="margin:0; padding:0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">
        <table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#E8E8E8" style="border-collapse:collapse;">
            <tr>
                <td>
                    <!-- Preheader Text -->
                    <div style="display:none;font-size:1px;color:#E8E8E8;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide: all;">
                        <?= @$title ?> - <?= @$subject ?>
                    </div>
                    <!-- \Preheader Text -->

                    <!--[if (gte mso 9)|(IE)]>
                    <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                    <![endif]-->

                    <!-- Wrapper -->
                    <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="max-width:600px; margin:auto;" class="email-container">
                        <tr>
                            <td>
                                <!-- Logo + Links -->
                                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="middle" style="padding:10px; text-align:left;" width="115">
                                            <a href="<?= @$website ?>">
                                                <img src="<?= @$logo ?>" alt="<?= @$title ?> Logo (Image)"  width="115" height="104" border="0" align="left">
                                            </a>
                                        </td>
                                        <td valign="middle" style="padding:10px; text-align:right; line-height:1.1; font-family: sans-serif; font-size: 11px; font-weight:bold; color: #999999;">
                                            <?= @$subject ?>
                                        </td>
                                    </tr>
                                </table>
                                <!-- \Logo + Links-->

                                <!-- Main Body -->
                                <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                                    <tr>
                                        <td style="padding: 2%;">
                                            <?= @$body ?>
                                        </td>
                                    </tr>
                                </table>
                                <!-- \Main Body -->
                            </td>
                        </tr>
                        <!-- Footer -->
                        <tr>
                            <td style="text-align:center; padding:4% 0; font-family:sans-serif; font-size:12px; line-height:1.2; color:#666666;">
                                <?= @$footer ?>
                                <br>
                            </td>
                        </tr>
                        <!-- \Footer -->
                    </table>
                    <!-- \Wrapper -->

                    <!-- End of Outlook-specific -->
                    <!--[if (gte mso 9)|(IE)]>
                            </td>
                        </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
        </table>
    </body>
</html>